#!/bin/bash --login
sudo apt install curl
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update
sudo apt-get install git-core zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev software-properties-common libffi-dev nodejs yarn
cd
git clone https://github.com/rbenv/rbenv.git ~/.rbenv
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(rbenv init -)"' >> ~/.bashrc
exec $SHELL
git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
echo 'export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' >> ~/.bashrc
exec $SHELL
rbenv install 2.5.7
rbenv global 2.5.7
ruby -v
gem install bundler
git config --global color.ui true
#git config --global user.name "YOUR NAME"
#git config --global user.email "YOUR@EMAIL.com"
#ssh-keygen -t rsa -b 4096 -C "YOUR@EMAIL.com"
#cat ~/.ssh/id_rsa.pub
#ssh -T git@github.com
gem install rails -v 5.2.3
rails -v
sudo apt install postgresql-11 libpq-dev
sudo -u postgres createuser alpha -s
# If you would like to set a password for the user, you can do the following
#sudo -u postgres psql
#postgres=# \password chris
#### If you want to use SQLite (not recommended)
