#!/bin/bash --login
sudo apt install curl
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update
sudo apt-get install git-core zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev software-properties-common libffi-dev nodejs yarn
sudo apt-get install libgdbm-dev libncurses5-dev automake libtool bison libffi-dev
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
curl -sSL https://get.rvm.io | bash -s stable
#source /usr/local/rvm/bin/rvm
export PATH="$PATH:/usr/local/rvm/bin" # Add RVM to PATH for scripting
[[ -s "/usr/local/rvm/bin/rvm" ]] && source "/usr/local/rvm/bin/rvm"
rvm install 2.5.7
type rvm | head -n 1
rvm use 2.5.7 --default
ruby -v
gem install bundler
git config --global color.ui true
#git config --global user.name "YOUR NAME"
#git config --global user.email "YOUR@EMAIL.com"
#ssh-keygen -t rsa -b 4096 -C "YOUR@EMAIL.com"
#cat ~/.ssh/id_rsa.pub
#ssh -T git@github.com
gem install rails -v 5.2.3
rbenv rehash
rails -v
sudo apt install postgresql-11 libpq-dev
sudo -u postgres createuser alpha -s
# If you would like to set a password for the user, you can do the following
#sudo -u postgres psql
#postgres=# \password chris
#### If you want to use SQLite (not recommended)
rails new myapp
#### If you want to use Postgres
# Note that this will expect a postgres user with the same username
# as your app, you may need to edit config/database.yml to match the
# user you created earlier
rails new myapp -d postgresql
# Move into the application directory
cd myapp
# If you setup MySQL or Postgres with a username/password, modify the
# config/database.yml file to contain the username/password that you specified
# Create the database
rake db:create
rails server
