// Configure the Google Cloud provider
provider "google" {
 credentials = "${file("LabResearch-ed61eff31fb3.json")}"
 project     = "research-254507"
 region      = "us-west2"
}
// Terraform plugin for creating random ids
resource "random_id" "instance_id" {
 byte_length = 8
}

// A single Google Cloud Engine instance
resource "google_compute_instance" "default" {
 name         = "deploy-terraform-${random_id.instance_id.hex}"
 machine_type = "g1-small"
 zone         = "us-west2-a"

 boot_disk {
   initialize_params {
     image = "gce-uefi-images/ubuntu-1804-lts"
   }
 }

// Make sure flask is installed on all new instances for later steps
 metadata_startup_script = "sudo apt-get update; sudo apt-get install tomcat8"

 network_interface {
   network = "default"

   access_config {
     // Include this section to give the VM an external ip address
   }
 }
}
// A variable for extracting the external ip of the instance
output "ip" {
 value = "${google_compute_instance.default.network_interface.0.access_config.0.nat_ip}"
}
